// jQuery UI Touch Punch 0.2.3 - must load after jQuery UI
// enables touch support for jQuery UI
! function (a) {
	function f(a, b) {
		if (!(a.originalEvent.touches.length > 1)) {
			a.preventDefault();
			var c = a.originalEvent.changedTouches[0],
				d = document.createEvent("MouseEvents");
			d.initMouseEvent(b, !0, !0, window, 1, c.screenX, c.screenY, c.clientX, c.clientY, !1, !1, !1, !1, 0, null), a.target.dispatchEvent(d)
		}
	}
	if (a.support.touch = "ontouchend" in document, a.support.touch) {
		var e, b = a.ui.mouse.prototype,
			c = b._mouseInit,
			d = b._mouseDestroy;
		b._touchStart = function (a) {
			var b = this;
			!e && b._mouseCapture(a.originalEvent.changedTouches[0]) && (e = !0, b._touchMoved = !1, f(a, "mouseover"), f(a, "mousemove"), f(a, "mousedown"))
		}, b._touchMove = function (a) {
			e && (this._touchMoved = !0, f(a, "mousemove"))
		}, b._touchEnd = function (a) {
			e && (f(a, "mouseup"), f(a, "mouseout"), this._touchMoved || f(a, "click"), e = !1)
		}, b._mouseInit = function () {
			var b = this;
			b.element.bind({
				touchstart: a.proxy(b, "_touchStart"),
				touchmove: a.proxy(b, "_touchMove"),
				touchend: a.proxy(b, "_touchEnd")
			}), c.call(b)
		}, b._mouseDestroy = function () {
			var b = this;
			b.element.unbind({
				touchstart: a.proxy(b, "_touchStart"),
				touchmove: a.proxy(b, "_touchMove"),
				touchend: a.proxy(b, "_touchEnd")
			}), d.call(b)
		}
	}
}(jQuery);

/** Range slider */
var slider = document.getElementById("grid_size");
var output = document.getElementById("grid_size__label");
output.innerHTML = slider.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function () {
	output.innerHTML = this.value;
}

/** Puzzle */
let fn = () => {


	function start_puzzle(x) {
		$('#puzzle_solved').hide();
		$('#source_image img').snapPuzzle({
			rows: x,
			columns: x,
			pile: '#pile',
			containment: '#puzzle-containment',
			onComplete: function () {
				console.log('start_puzzle -> onComplete');
				$('#source_image img').fadeOut(150).fadeIn();
				$('#puzzle_solved').show();
			}
		});
	}

	$('#pile').height($('#source_image img').height());

	let grid_size = 1;
	start_puzzle(grid_size);

	const restart = () => {
		// $('#source_image img').snapPuzzle('destroy');
		$('#pile').height($('#source_image img').height());
		start_puzzle(grid_size);

	}

	$('.restart-puzzle').click(function () {
		grid_size = $(this).data('grid') !== 0 ? $(this).data('grid') : grid_size;
		$('#source_image img').snapPuzzle('destroy');
		restart();
	});

	$('#grid_size').change(function () {
		grid_size = $('#grid_size').get()[0].value ? $('#grid_size').get()[0].value : grid_size;
		$('#source_image img').snapPuzzle('destroy');
		restart();
	});

	$(window).resize(function () {
		$('#pile').height($('#source_image img').height());
		$('#source_image img').snapPuzzle('refresh');
	});

}
document.addEventListener('DOMContentLoaded', fn, false);