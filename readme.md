Motherboard Puzzle 
====================
by Malyarevich

[![pipeline status](https://gitlab.com/malyarevich/motherboardpuzzle/badges/master/pipeline.svg)](https://gitlab.com/malyarevich/motherboardpuzzle/commits/master)
## Features

* Adjustable difficulty level
* May be used in responsive designs
* Drag and drop
* Callbacks

## Changelog

### Version 1.0.0 - 2014/12/17

* Initial release
